const express = require('express');
const taskRoutes = require('./server/tasks/task.route');

const router = express.Router();

router.use('/tasks', taskRoutes);

module.exports = router;
