const Task = require('./task.model');

async function getAll(req, res, next) {
  try {
    const tasks = await Task.list();
    res.json(tasks);
  } catch (err) {
    next(err);
  }
}

async function create(req, res, next) {
  try {
    const task = await Task.create(req.body);
    res.json(task);
  } catch (err) {
    next(err);
  }
}

async function get(req, res, next) {
  try {
    const tasks = await Task.get(req.params.id);
    res.json(tasks[0]);
  } catch (err) {
    next(err);
  }
}

async function update(req, res, next) {
  try {
    const task = await Task.update(req.params.id, req.body);
    res.json(task);
  } catch (err) {
    next(err);
  }
}

async function remove(req, res, next) {
  try {
    await Task.remove(req.params.id);
    res.json({});
  } catch (err) {
    next(err);
  }
}

async function check(req, res, next) {
  try {
    const task = await Task.check(req.params.id, req.body);
    res.json(task);
  } catch (err) {
    next(err);
  }
}

module.exports = { getAll, create, get, update, remove, check };
