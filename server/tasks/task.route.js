const express = require('express');
const taskCtrl = require('./task.controller');

const router = express.Router();

router.route('/')
  .get((res, req, next) => next(), taskCtrl.getAll)
  .post((res, req, next) => next(), taskCtrl.create);

router.route('/:id')
  .get((res, req, next) => next(), taskCtrl.get)
  .put((res, req, next) => next(), taskCtrl.update)
  .delete((res, req, next) => next(), taskCtrl.remove);

router.route('/check/:id')
  .post((res, req, next) => next(), taskCtrl.check);

module.exports = router;
