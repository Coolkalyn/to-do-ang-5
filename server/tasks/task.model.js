const mongoose = require('mongoose');
const ObjectId = require('mongodb').ObjectID;

/**
 * Activity Type Schema
 */
const TaskSchema = new mongoose.Schema({
  name: { type: String },
  description: { type: String },
  done: { type: Boolean, default: false }
}, {
  timestamps: true
});

TaskSchema.statics = {

  create(data) {
    const Task = new this(data);
    Task.done = false;
    return Task.save();
  },

  list() {
    return this.find();
  },

  check(id, data) {
    return this.findByIdAndUpdate(id, {$set: {done: data.done}});
  },

  update(id, data) {
    return this.findByIdAndUpdate(id, data);
  },

  get(id) {
    return this.find({"_id": ObjectId(id)});
  },

  remove(id) {
    return this.findByIdAndRemove(id);
  }

};

module.exports = mongoose.model('Task', TaskSchema);
