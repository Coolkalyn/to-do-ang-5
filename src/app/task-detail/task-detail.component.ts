import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TaskService } from '../task.service';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.css']
})
export class TaskDetailComponent implements OnInit {
  title: string;
  task: object;
  edit: boolean;
  id: string;

  constructor(
    private taskService: TaskService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    if (this.route.snapshot.routeConfig.path.indexOf('edit') !== -1) {
      this.title = 'Edit task';
      this.task = {};
      this.id = this.route.snapshot.paramMap.get('id');
      this.getTask(this.id);
      this.edit = true;
    } else {
      this.title = 'Add new task';
      this.task = {};
      this.edit = false;
    }
  }

  ngOnInit() {
  }

  getTask(id) {
    this.taskService.getTask(id).subscribe((response) => {
      this.task = response;
    });
  }

  save() {
    if (!this.edit) {
      this.taskService.create(this.task).subscribe((response) => {
        this.router.navigate(['/dashboard']);
      });
    } else {
      this.taskService.update(this.id, this.task).subscribe((response) => {
        this.router.navigate(['/dashboard']);
      });
    }
  }

}
