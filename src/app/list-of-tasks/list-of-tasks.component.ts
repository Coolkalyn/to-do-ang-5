import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TaskService } from '../task.service';

@Component({
  selector: 'app-list-of-tasks',
  templateUrl: './list-of-tasks.component.html',
  styleUrls: ['./list-of-tasks.component.css']
})
export class ListOfTasksComponent implements OnInit {

  tasks: any;

  constructor(
    private taskService: TaskService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getTasks();
  }

  getTasks(): void {
    this.taskService.getAllTasks().subscribe((response) => {
      this.tasks = response;
    });
  }

  check(id, check) {
    this.taskService.check(id, !check).subscribe(response => {
      this.getTasks();
    });
  }

  remove(id) {
    this.taskService.remove(id).subscribe(response => {
      this.getTasks();
    });
  }

  edit(id) {
    this.router.navigate(['/edit/' + id]);
  }

}
