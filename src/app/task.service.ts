import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class TaskService {

  constructor(public http: HttpClient) { }

  getAllTasks() {
    return this.http.get('/api/tasks');
  }

  getTask(id) {
    return this.http.get('/api/tasks/' + id);
  }

  create(data) {
    return this.http.post('/api/tasks', data);
  }

  update(id, data) {
    return this.http.put('/api/tasks/' + id, data);
  }

  check(id, data) {
    return this.http.post('/api/tasks/check/' + id, {done: data});
  }

  remove(id) {
    return this.http.delete('/api/tasks/' + id);
  }

}
