import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ListOfTasksComponent } from './list-of-tasks/list-of-tasks.component';
import { TaskDetailComponent } from './task-detail/task-detail.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: ListOfTasksComponent
  },
  {
    path: 'create',
    component: TaskDetailComponent
  },
  {
    path: 'edit/:id',
    component: TaskDetailComponent
  },
  {
    path: '**',
    redirectTo: '/dashboard',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
